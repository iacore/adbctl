
struct None {}
type Maybe<T> = None | T

fn (m Maybe<T>) get<T>() ?T {
	return match m {
		None { none }
		T { m }
	}
}
