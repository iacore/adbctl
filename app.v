import code0100fun.glfw3 as glfw { GLFWHints, GLFWOpenGLProfile, Size }
import sokol.gfx
import sokol.sgl
import stbi
import time { Duration, sleep }

struct App {
	pass_action gfx.PassAction
mut:
	size        Size
	main_window &glfw.GLFWwindow
	sg_context  gfx.Context
	adb_device  AdbDevice
	texture     Maybe<gfx.Image>
	chan_update chan stbi.Image
}

fn control_device(device AdbDevice) ?u8 {
	width, height := device.screen_size()?
	println('Display size: ${width}x${height}')

	mut app := &App{
		size: Size{width, height}
		main_window: 0
		pass_action: gfx.create_clear_pass(0.1, 0.1, 0.1, 1.0)
		adb_device: device
		texture: None{}
		chan_update: chan stbi.Image{cap: 4}
	}
	defer {
		app.free()
	}
	app.init()
	app.run()
	return 0
}

struct St {
mut:
	x u64 // data to be shared
}

fn screenshotting_service(run &bool, device AdbDevice, ch chan stbi.Image) {
	mut outstanding_threads := 0

	shared last_id := St{0}

	mut next_id := u64(0)
	for run {
		if outstanding_threads < 8 {
			outstanding_threads++
			go fn [device, ch] (mut outstanding_threads &int, id u64, shared last_id St) {
				img := device.capture_screen() or {
					eprintln('Capture screen failed: $err')
					return
				}
				mut send := false
				lock last_id {
					if id > last_id.x {
						last_id.x = id
						send = true
					}
				}
				if send {
					ch <- img
				}

				unsafe {
					(*outstanding_threads)--
				}
			}(mut &outstanding_threads, next_id, shared last_id)
		}

		next_id++
		sleep(Duration(10 * time.millisecond))
	}
}

fn (mut app App) init() {
	app.init_glfw()
	app.init_sg()

	sgl_desc := sgl.Desc{
		max_vertices: 50 * 65536
	}
	sgl.setup(&sgl_desc)
}

fn (mut app App) free() {
	app.main_window.destroy()
	glfw.terminate()
}

fn (mut app App) run() {
	mut run := true
	go screenshotting_service(&run, app.adb_device, app.chan_update)

	for run {
		glfw.poll_events()
		mut window := app.main_window
		window.make_context_current()

		select {
			new_img := <-app.chan_update {
				app.update_screenshot(new_img)
			}
			else {}
		}

		app.frame()

		if window.should_close() {
			run = false
		}
	}
}

fn (app App) window_size() Size {
	return app.size
}

fn (mut app App) init_sg() {
	desc := gfx.Desc{}
	gfx.setup(&desc)
	app.sg_context = gfx.setup_context()
	app.texture = gfx.make_image(gfx.ImageDesc{
		width: app.size.width
		height: app.size.height
		pixel_format: .rgba8
		usage: .dynamic
		wrap_u: .clamp_to_edge
		wrap_v: .clamp_to_edge
		min_filter: .nearest
		mag_filter: .nearest
		num_mipmaps: 1
		num_slices: 1
	})
}

fn (mut app App) frame() {
	app.main_window.make_context_current()
	gfx.activate_context(app.sg_context)

	size := app.main_window.get_framebuffer_size()
	app.size.width = size.width
	app.size.height = size.height

	gfx.begin_default_pass(&app.pass_action, app.size.width, app.size.height)
	sgl.default_pipeline()

	sgl.viewport(0, 0, app.size.width, app.size.height, true)
	app.draw_screenshot()

	sgl.draw()
	gfx.end_pass()
	gfx.commit()

	app.main_window.swap_buffers()
}

fn (mut app App) init_glfw() {
	glfw.glfw_init()
	glfw.window_hint(GLFWHints.version_major, 3)
	glfw.window_hint(GLFWHints.version_minor, 3)
	glfw.window_hint(GLFWHints.opengl_forward_compat, 1)
	glfw.window_hint(GLFWHints.opengl_profile, GLFWOpenGLProfile.core)
	glfw.window_hint(GLFWHints.resizable, 0)
	size := app.window_size()
	win_opts := glfw.CreateWindowOptions{
		width: size.width
		height: size.height
		title: 'adbctl'
		monitor: 0
		share: 0
	}
	mut window := glfw.create_window(win_opts)
	window.set_user_pointer(&app)
	window.set_key_callback(handle_key_events)
	window.set_cursor_pos_callback(handle_cursor_pos_events)
	window.set_mouse_button_callback(handle_mouse_button_events)
	window.make_context_current()
	window.set_pos(100, 100)

	glfw.swap_interval(1)
	app.main_window = window
}

fn (app App) draw_screenshot() {
	sgl.defaults()
	sgl.enable_texture()
	sgl.texture(app.texture.get() or { panic('no texture loaded') })
	sgl.begin_quads()
	verts := [[-1, 1, 0, 0], [1, 1, 1, 0], [1, -1, 1, 1], [-1, -1, 0, 1]]
	for vert in verts {
		sgl.v2f_t2f(vert[0], vert[1], vert[2], vert[3])
	}
	sgl.end()
}

fn (mut app App) update_screenshot(img stbi.Image) {
	texture := app.texture.get() or { panic('no texture loaded') }
	mut data := gfx.ImageData{}
	data.subimage[0][0].ptr = img.data
	data.subimage[0][0].size = usize(img.width * img.height * img.nr_channels)
	gfx.update_image(texture, &data)
}

fn handle_key_events(window &glfw.GLFWwindow, key int, scancode int, action int, mods int) {
	mut app := get_window_app(window)
	_ = app
	// Handle event with app context
}

fn handle_cursor_pos_events(window &glfw.GLFWwindow, xpos f64, ypos f64) {
	mut app := get_window_app(window)
	_ = app
	// Handle event with app context
}

fn handle_mouse_button_events(window &glfw.GLFWwindow, button int, action int, mods int) {
	mut app := get_window_app(window)
	_ = app
	// Handle event with app context
}

fn get_window_app(window &glfw.GLFWwindow) &App {
	return &App(window.get_user_pointer())
}
