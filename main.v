module main

import readline { read_line }
import strconv { atoi }

// interactive
fn choose_device(devices []AdbDevice) ?AdbDevice {
	for i, device in devices {
		println('Choose your device:')
		println('${i:2} $device.id $device.attrs')
	}
	input := read_line('Your choice: ')?
	return devices[atoi(input)?]?
}

fn main() {
	adb := Adb{
		arg0: '/usr/bin/adb'
	}
	devices := adb.devices()?
	device := match devices.len {
		1 {
			default_device := devices[0]
			println('Selecting the only device: ${default_device.id} ${default_device.attrs} ')
			default_device
		}
		0 {
			println('no devices')
			exit(1)
		}
		else {
			choose_device(devices)?
		}
	}

	exit(control_device(device)?)
}
