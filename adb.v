import os
import regex
import strconv { atoi }
import stbi

// the `adb` command
pub struct Adb {
	arg0 string
}

// android device
pub struct AdbDevice {
	adb   &Adb
	id    string
	@type string // 'device' or ...
	attrs map[string]string
}


// read from filedescriptor, block until data
fn fd_slurp(fd int) []u8 {
	mut res := []u8{}
	if fd == -1 {
		return res
	}
	for {
		s, b := fd_read(fd, 4096)
		if b <= 0 {
			break
		}
		res << s
	}
	return res
}

// read from filedescriptor, don't block
// return [bytestring,nrbytes]
fn fd_read(fd int, maxbytes int) ([]u8, int) {
	mut buf := []u8{len: maxbytes, cap: maxbytes}
	unsafe {
		nbytes := C.read(fd, &buf[0], buf.len)
		if nbytes < 0 {
			return []u8{}, nbytes
		}
		return buf[0..nbytes], nbytes
	}
}

pub fn (a Adb) run_command(args []string) ?[]u8 {
	mut p := os.Process{
		filename: a.arg0
		args: args
	}
	p.set_redirect_stdio()
	p.run()
	mut buf := fd_slurp(p.stdio_fd[1])
	
	return buf
}

pub fn (a Adb) devices() ?[]AdbDevice {
	output := a.run_command(['devices', '-l'])?

	mut devices := []AdbDevice{}
	for i, line in output.bytestr().split_into_lines() {
		if i == 0 || line == '' {
			continue
		}
		fields := line.fields()
		if fields.len < 2 {
			return error('Invalid adb response: $line')
		}
		id := fields[0]
		dev_type := fields[1]
		mut attrs := map[string]string{}
		for attr_pair in fields[2..] {
			tokens := attr_pair.split_nth(':', 2)
			match tokens.len {
				2 {
					attrs[tokens[0]] = tokens[1]
				}
				else {
					return error('Invalid adb response - device attr: $attr_pair')
				}
			}
		}
		devices << AdbDevice{
			adb: &a
			id: id
			@type: dev_type
			attrs: attrs
		}
	}

	return devices
}

pub fn (device AdbDevice) capture_screen_raw() ?[]u8 {
	stdout := device.adb.run_command(['-s', device.id, 'shell', 'screencap', '-p'])?
	return stdout
}

// todo: find a faster way to stream screen
// take a screenshot
pub fn (device AdbDevice) capture_screen() ?stbi.Image {
	stdout := device.adb.run_command(['-s', device.id, 'shell', 'screencap', '-p'])?
	img := stbi.load_from_memory(stdout.data, stdout.len)?
	return img
}

// swipe a line segment (2 points)
pub fn (device AdbDevice) swipe_line(duration_in_ms f32, p0 Vec2, p1 Vec2) ? {
	_ = device.adb.run_command(["-s", device.id, "shell", "input", "swipe", p0.x.str(), p0.y.str(), p1.x.str(), p1.y.str(), duration_in_ms.str()])?
}

// tap the screen
pub fn (device AdbDevice) tap(at Vec2) ? {
	_ = device.adb.run_command(["-s", device.id, "shell", "input", "tap", at.x.str(), at.y.str()])?
}

// send a keyboard key, or Android-specific key like home/back/etc
pub fn (device AdbDevice) press_key(keycode int) ? {
	_ = device.adb.run_command(["-s", device.id, "shell", "input", "keyevent", keycode.str()])?
}

pub fn (device AdbDevice) screen_size() ?(int, int) {
	output := device.adb.run_command(["-s", device.id, "shell", "wm", "size"])?.bytestr()
	mut re := regex.regex_opt(r"Physical size: (\d+)x(\d+)") or { panic('wtf') }
	if !re.matches_string(output) {
		return error('Invalid adb output: $output')
	}
	groups := re.get_group_list()

	getnumber := fn [output] (group regex.Re_group) ?int {
		return atoi(output[group.start..group.end])
	}

	return getnumber(groups[0])?, getnumber(groups[1])?
}
